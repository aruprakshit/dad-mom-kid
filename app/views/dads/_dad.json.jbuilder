json.extract! dad, :id, :name, :created_at, :updated_at
json.url dad_url(dad, format: :json)
