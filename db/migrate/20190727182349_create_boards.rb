class CreateBoards < ActiveRecord::Migration[5.2]
  def change
    create_table :boards do |t|
      t.integer :capacity
      t.string :name

      t.timestamps
    end
  end
end
