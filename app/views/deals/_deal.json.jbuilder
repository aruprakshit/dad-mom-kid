json.extract! deal, :id, :name, :created_at, :updated_at
json.url deal_url(deal, format: :json)
