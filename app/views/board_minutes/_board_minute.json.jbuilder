json.extract! board_minute, :id, :deal_id, :board_id, :quantity, :created_at, :updated_at
json.url board_minute_url(board_minute, format: :json)
