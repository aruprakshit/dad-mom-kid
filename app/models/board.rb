class Board < ApplicationRecord
  has_many :board_minutes
  has_many :deals, through: :board_minutes

  scope :unassigned_to_deal,
    ->(deal_id) { where.not(id: Board.joins(:deals).where("deals.id = ?", deal_id)) }
end
