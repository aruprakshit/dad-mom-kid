class Child < ApplicationRecord
  belongs_to :mom, optional: true
  belongs_to :dad, optional: true
end
