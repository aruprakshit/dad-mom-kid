require "application_system_test_case"

class BoardMinutesTest < ApplicationSystemTestCase
  setup do
    @board_minute = board_minutes(:one)
  end

  test "visiting the index" do
    visit board_minutes_url
    assert_selector "h1", text: "Board Minutes"
  end

  test "creating a Board minute" do
    visit board_minutes_url
    click_on "New Board Minute"

    fill_in "Board", with: @board_minute.board_id
    fill_in "Deal", with: @board_minute.deal_id
    fill_in "Quantity", with: @board_minute.quantity
    click_on "Create Board minute"

    assert_text "Board minute was successfully created"
    click_on "Back"
  end

  test "updating a Board minute" do
    visit board_minutes_url
    click_on "Edit", match: :first

    fill_in "Board", with: @board_minute.board_id
    fill_in "Deal", with: @board_minute.deal_id
    fill_in "Quantity", with: @board_minute.quantity
    click_on "Update Board minute"

    assert_text "Board minute was successfully updated"
    click_on "Back"
  end

  test "destroying a Board minute" do
    visit board_minutes_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Board minute was successfully destroyed"
  end
end
