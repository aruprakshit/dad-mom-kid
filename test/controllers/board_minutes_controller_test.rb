require 'test_helper'

class BoardMinutesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @board_minute = board_minutes(:one)
  end

  test "should get index" do
    get board_minutes_url
    assert_response :success
  end

  test "should get new" do
    get new_board_minute_url
    assert_response :success
  end

  test "should create board_minute" do
    assert_difference('BoardMinute.count') do
      post board_minutes_url, params: { board_minute: { board_id: @board_minute.board_id, deal_id: @board_minute.deal_id, quantity: @board_minute.quantity } }
    end

    assert_redirected_to board_minute_url(BoardMinute.last)
  end

  test "should show board_minute" do
    get board_minute_url(@board_minute)
    assert_response :success
  end

  test "should get edit" do
    get edit_board_minute_url(@board_minute)
    assert_response :success
  end

  test "should update board_minute" do
    patch board_minute_url(@board_minute), params: { board_minute: { board_id: @board_minute.board_id, deal_id: @board_minute.deal_id, quantity: @board_minute.quantity } }
    assert_redirected_to board_minute_url(@board_minute)
  end

  test "should destroy board_minute" do
    assert_difference('BoardMinute.count', -1) do
      delete board_minute_url(@board_minute)
    end

    assert_redirected_to board_minutes_url
  end
end
