class CreateBoardMinutes < ActiveRecord::Migration[5.2]
  def change
    create_table :board_minutes do |t|
      t.references :deal, foreign_key: true
      t.references :board, foreign_key: true
      t.integer :quantity

      t.timestamps
    end
  end
end
