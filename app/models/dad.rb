class Dad < ApplicationRecord
  has_many :children
  has_many :moms, through: :children

  accepts_nested_attributes_for :children

  def update_children_attributes(children_attributes)
    children_attributes.each do |_, attrs|
      attrs['dad_id'] = self.id if attrs['dad_id'] == "0"

      if attrs['dad_id'].blank?
        attrs['dad_id'] = nil
        attrs['age'] = nil
      end

      Child.find(attrs['id']).update(attrs.slice(:dad_id, :age))
    end
  end
end
