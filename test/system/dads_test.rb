require "application_system_test_case"

class DadsTest < ApplicationSystemTestCase
  setup do
    @dad = dads(:one)
  end

  test "visiting the index" do
    visit dads_url
    assert_selector "h1", text: "Dads"
  end

  test "creating a Dad" do
    visit dads_url
    click_on "New Dad"

    fill_in "Name", with: @dad.name
    click_on "Create Dad"

    assert_text "Dad was successfully created"
    click_on "Back"
  end

  test "updating a Dad" do
    visit dads_url
    click_on "Edit", match: :first

    fill_in "Name", with: @dad.name
    click_on "Update Dad"

    assert_text "Dad was successfully updated"
    click_on "Back"
  end

  test "destroying a Dad" do
    visit dads_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Dad was successfully destroyed"
  end
end
