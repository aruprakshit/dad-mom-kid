class Mom < ApplicationRecord
  has_many :children
  has_many :dads, through: :children
end
