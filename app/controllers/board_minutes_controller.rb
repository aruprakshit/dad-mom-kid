class BoardMinutesController < ApplicationController
  before_action :set_board_minute, only: [:show, :edit, :update, :destroy]

  # GET /board_minutes
  # GET /board_minutes.json
  def index
    @board_minutes = BoardMinute.all
  end

  # GET /board_minutes/1
  # GET /board_minutes/1.json
  def show
  end

  # GET /board_minutes/new
  def new
    @board_minute = BoardMinute.new
  end

  # GET /board_minutes/1/edit
  def edit
  end

  # POST /board_minutes
  # POST /board_minutes.json
  def create
    @board_minute = BoardMinute.new(board_minute_params)

    respond_to do |format|
      if @board_minute.save
        format.html { redirect_to @board_minute, notice: 'Board minute was successfully created.' }
        format.json { render :show, status: :created, location: @board_minute }
      else
        format.html { render :new }
        format.json { render json: @board_minute.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /board_minutes/1
  # PATCH/PUT /board_minutes/1.json
  def update
    respond_to do |format|
      if @board_minute.update(board_minute_params)
        format.html { redirect_to @board_minute, notice: 'Board minute was successfully updated.' }
        format.json { render :show, status: :ok, location: @board_minute }
      else
        format.html { render :edit }
        format.json { render json: @board_minute.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /board_minutes/1
  # DELETE /board_minutes/1.json
  def destroy
    @board_minute.destroy
    respond_to do |format|
      format.html { redirect_to board_minutes_url, notice: 'Board minute was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_board_minute
      @board_minute = BoardMinute.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def board_minute_params
      params.require(:board_minute).permit(:deal_id, :board_id, :quantity)
    end
end
