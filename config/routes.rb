Rails.application.routes.draw do
  resources :deals
  resources :boards
  resources :children
  resources :moms
  resources :dads

  # root to: "dads#index"
  root to: "boards#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
