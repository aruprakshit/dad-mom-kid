class Deal < ApplicationRecord
  has_many :board_minutes
  has_many :boards, through: :board_minutes

  accepts_nested_attributes_for :board_minutes,
    reject_if: proc { |attributes| attributes['quantity'].blank? }
end
